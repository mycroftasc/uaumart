﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace uaumart.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Seller> Sellers { get; set; } = new List<Seller>();

        /*Método construtor vazio*/
        public Department() { }

        /*Método contrutor com paramêtros*/
        public Department(int id, string name)
        {
            Id = id;
            Name = name;
        }
        /*Adiciona vendedor na lista de vendedores*/
        public void AddSeller(Seller seller) {
            Sellers.Add(seller);
        }

        public double TotalSales(DateTime initial, DateTime final) {
            return Sellers.Sum(seller => seller.TotalSales(initial, final));
        }
    }
}
