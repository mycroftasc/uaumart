﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using uaumart.Models;
using uaumart.Data;
using uaumart.Services;

namespace uaumart
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<uaumartContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("uaumartContext")));
            /*Usando na lista de serviços, assim podemos injetar meu serviço em outros*/
            services.AddScoped<SeedingService>();
            /*Adiciona na lista de serviços o meu serviço
                que acessa o banco - Tabela seller*/
            services.AddScoped<SellerService>();
            services.AddScoped<DepartmentService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /*O método Configure() permite que eu passe argumentos adicionais.
         Vamos passar como argumento o SeedService.
        */
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, SeedingService seed)
        {
            /*Testa se o ambiente é de desenvolvimento*/
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                /*Aqui é chamado o método para popular o banco de dados*/
               seed.Seed();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
