﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using uaumart.Models;

namespace uaumart.Services
{
    //Essa é a classe responsável por acessar a base de dados
    public class SellerService
    {
        private readonly uaumartContext _context;

        public SellerService(uaumartContext context) {
            _context = context;
        }

        public List<Seller> FindAll() {
            /*Seleciona os objetos da tabela Seller e transforma em uma lista.
             Por enquanto esse método está sincrono, ou seja, quando essa operação
             for xecutada a aplicação vai ficar "parada/aguardando" um retorno.             
             */ 
            return _context.Seller.ToList();
        }

        public void Insert(Seller obj) {
            /*Solução alternativa e temporária para enquanto eu não tiver o 
            componente para selecionar o departamento
            obj.Department = _context.Department.First();*/
            /*adiciona registro*/
            _context.Add(obj);

            /*persiste o registro*/
            _context.SaveChanges();
        }
    }
}
