﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using uaumart.Models;

namespace uaumart.Services
{
    public class DepartmentService
    {
        private readonly uaumartContext _context;

        public DepartmentService(uaumartContext context)
        {
            _context = context;
        }

        public List<Department> FindAll() {
            return _context.Department.OrderBy(register => register.Name).ToList();
        }
    }
}
