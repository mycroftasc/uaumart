﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace uaumart.Models
{
    public class uaumartContext : DbContext
    {
        public uaumartContext (DbContextOptions<uaumartContext> options)
            : base(options)
        {
        }

        public DbSet<uaumart.Models.Department> Department { get; set; }
        public DbSet<uaumart.Models.Seller> Seller { get; set; }
        public DbSet<uaumart.Models.SalesRecord> SalesRecord { get; set; }
    }
}
