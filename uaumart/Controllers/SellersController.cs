﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using uaumart.Models;
using uaumart.Models.ViewModel;
using uaumart.Services;

namespace uaumart.Controllers
{
    public class SellersController : Controller
    {
        //Injeção de dependência
        private readonly SellerService _sellerService;
        private readonly DepartmentService _departmentService;

        //Construtor para finalizar a injeção de dependência
        public SellersController(SellerService sellerService, DepartmentService departmentService) {
            _sellerService = sellerService;
            _departmentService = departmentService;
        }

        public IActionResult Index()
        {
            //acessa o serviço e chama o método que retorna a lista de sellers
            var list = _sellerService.FindAll();
            return View(list);
        }
        /*GET*/
        public IActionResult Create() {
            /*Carrega department*/
            var departments = _departmentService.FindAll();

            /*Instância do nosso ViewModel SellerForViewModel
              que vai ter duas propriedades/atributos
              a primeira propriedade é a lista de departamentos
              que já temos e a segunda é Sellers
             */
            var viewModel = new SellerFormViewModel { Departments = departments};
            return View(viewModel);
        }
        /*A notação HttpPost indoca que essa action é do tipo POST*/
        [HttpPost]
        public IActionResult Create(Seller seller) {
            _sellerService.Insert(seller);
            /*Chama a action Index deste controlador*/
            return RedirectToAction("Index");
        }

    }
}